package com.classpath.inventorymicroservice.model;

public enum EventType {
    ORDER_ACCEPTED,
    ORDER_CANCELLED,
    ORDER_REJECTED,
    ORDER_FULFILLED
}
